package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSAMLToken;
import org.apache.ws.security.saml.ext.AssertionWrapper;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.impl.KeyInfoBuilder;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;
import org.springframework.xml.transform.TransformerObjectSupport;
import org.w3c.dom.Document;

import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.AuthenticationMethodSchemaCustom;
import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.AuthenticationMethodSchemaCustomBuilder;
import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.KeyAliasCustom;
import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.KeyAliasCustomBuilder;
import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.KeyValueCustom;
import br.com.bancooriginal.crmd.bootstrap.autenticacao.security.KeyValueCustomBuilder;
import net.technisys.cmm.services.metadata.v2.AbstractEnumComplexType;
import net.technisys.cmm.services.metadata.v2.Metadata;
import net.technisys.cmm.services.metadata.v2.ObjectFactory;

public class CallBackSecurityWss extends TransformerObjectSupport implements WebServiceMessageCallback {

    private AutenticacaoLdap autenticacao;
    private String serviceId;
    private String system;

    /**
     * Construtor
     */
    public CallBackSecurityWss(String serviceId, String system, AutenticacaoLdap autenticacao) {
        super();
        this.autenticacao = autenticacao;
        this.serviceId = serviceId;
        this.system = system;
    }

    private static final String ISSUE_DEFAULT = "CyberbankMultichannelManager";
    private static final String NAME_ID_FORMAT_DEFAULT = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity";

    private final Propriedades propriedade = new Propriedades();

    @SuppressWarnings("rawtypes")
    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
        final ObjectFactory objectFactory = new ObjectFactory();
        /**
         * Campos Parametros
         */
        AbstractEnumComplexType channel = new AbstractEnumComplexType();
        channel.setMnemonic(system);

        Metadata metadata = objectFactory.createMetadata();
        metadata.setServiceId(serviceId);
        metadata.setSessionId(autenticacao.getSessionID());
        String userId = autenticacao.getUserID() != null ? autenticacao.getUserID()
                : autenticacao.getExecutingOperatorId();
        JAXBElement<String> userIdJAXBElement = this.criarElementoJAXB("executingOperatorId", userId);
        metadata.setExecutingOperatorId(userIdJAXBElement);
        metadata.setAddress(autenticacao.getIp());

        /**
         * Fixos
         */
        metadata.setServiceVersion("1.0");
        JAXBElement<AbstractEnumComplexType> channelJAXBElement = this.criarElementoJAXB("executingChannel",
                channel);
        metadata.setExecutingChannel(channelJAXBElement);
        JAXBElement<String> institutionIDJAXBElement = this.criarElementoJAXB("institutionId", "1111");
        metadata.setInstitutionId(institutionIDJAXBElement);
        JAXBElement<String> organizationIDJAXBElement = this.criarElementoJAXB("organizationId", "1111");
        metadata.setOrganizationId(organizationIDJAXBElement);
        JAXBElement<String> branchIDJAXBElement = this.criarElementoJAXB("branchId", "1");
        metadata.setBranchId(branchIDJAXBElement);
        metadata.setLocale("pt_BR");
        JAXBElement<String> terminalIDJAXBElement = this.criarElementoJAXB("terminalId", "ws");
        metadata.setTerminalId(terminalIDJAXBElement);

        try {

            DefaultBootstrap.bootstrap();
            XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();

            /**
             * KeyInfo - Begin
             */

            // Create the Custom KeyAlias
            KeyAliasCustomBuilder keyAliasBuilder = new KeyAliasCustomBuilder();
            builderFactory.registerBuilder(KeyAliasCustom.DEFAULT_ELEMENT_NAME, keyAliasBuilder);
            keyAliasBuilder = (KeyAliasCustomBuilder) builderFactory.getBuilder(KeyAliasCustom.DEFAULT_ELEMENT_NAME);
            KeyAliasCustom keyAlias = (KeyAliasCustom) keyAliasBuilder.buildObject();
            keyAlias.setValue(autenticacao.getUsername());

            // Create the Custom KeyValue
            KeyValueCustomBuilder keyValueBuilder = new KeyValueCustomBuilder();
            builderFactory.registerBuilder(KeyValueCustom.DEFAULT_ELEMENT_NAME, keyValueBuilder);
            keyValueBuilder = (KeyValueCustomBuilder) builderFactory.getBuilder(KeyValueCustom.DEFAULT_ELEMENT_NAME);
            KeyValueCustom keyValue = (KeyValueCustom) keyValueBuilder.buildObject();
            keyValue.setValue(autenticacao.getPassword());

            // Create the KeyInfo
            KeyInfoBuilder keyInfoBuilder = (KeyInfoBuilder) builderFactory.getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME);
            KeyInfo keyInfo = (KeyInfo) keyInfoBuilder.buildObject();
            keyInfo.getXMLObjects().add(keyAlias);

            // caso nao tenha sido informado a senha nao serÃ¡ enviado para o
            // servico
            if (autenticacao.getPassword() != null) {
                keyInfo.getXMLObjects().add(keyValue);
            }

            /**
             * KeyInfo - End
             */

            // Create the custom AuthenticationMethodSchema
            SAMLObjectBuilder authenticationMethodSchemaBuilder = new AuthenticationMethodSchemaCustomBuilder();
            builderFactory.registerBuilder(AuthenticationMethodSchemaCustom.DEFAULT_ELEMENT_NAME,
                    authenticationMethodSchemaBuilder);
            authenticationMethodSchemaBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(AuthenticationMethodSchemaCustom.DEFAULT_ELEMENT_NAME);
            AuthenticationMethodSchemaCustom authenticationMethodSchema = (AuthenticationMethodSchemaCustom) authenticationMethodSchemaBuilder
                    .buildObject();
            authenticationMethodSchema.setValue(autenticacao.getAuthenticationMethodSchema());

            // Create the SubjectConfirmationData
            SAMLObjectBuilder subjectConfirmationDataBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME);
            SubjectConfirmationData subjectConfirmationData = (SubjectConfirmationData) subjectConfirmationDataBuilder
                    .buildObject();
            subjectConfirmationData.setRecipient("true");
            subjectConfirmationData.getUnknownXMLObjects().add(keyInfo);
            subjectConfirmationData.getUnknownXMLObjects().add(authenticationMethodSchema);

            // Create the SubjectConfirmation
            SAMLObjectBuilder subjectConfirmationBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
            SubjectConfirmation subjectConfirmation = (SubjectConfirmation) subjectConfirmationBuilder.buildObject();
            subjectConfirmation.setSubjectConfirmationData(subjectConfirmationData);
            subjectConfirmation.setMethod(autenticacao.getAuthenticationType());

            SAMLObjectBuilder nameIDBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(NameID.DEFAULT_ELEMENT_NAME);
            NameID nameID = (NameID) nameIDBuilder.buildObject();
            nameID.setFormat(NAME_ID_FORMAT_DEFAULT);
            nameID.setValue(autenticacao.getUsername());

            // Create the Subject
            SAMLObjectBuilder subjectBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Subject.DEFAULT_ELEMENT_NAME);
            Subject subject = (Subject) subjectBuilder.buildObject();
            subject.getSubjectConfirmations().add(subjectConfirmation);
            subject.setNameID(nameID);

            // Create the assertion
            SAMLObjectBuilder issuerBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
            Issuer issuer = (Issuer) issuerBuilder.buildObject();
            issuer.setValue(ISSUE_DEFAULT);

            // Create the assertion
            SAMLObjectBuilder assertionBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
            Assertion assertion = (Assertion) assertionBuilder.buildObject();
            assertion.setIssuer(issuer);
            assertion.setSubject(subject);
            assertion.setIssueInstant(new DateTime());
            assertion.setVersion(SAMLVersion.VERSION_20);

            AssertionWrapper assertionWrapper = new AssertionWrapper(assertion);

            JAXBElement<Metadata> metadataJAXB = objectFactory.createMetadata(metadata);

            // Seta o marshaller
            final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
            marshaller.setPackagesToScan(propriedade.valor("marshaller.metadata.package.scan"));

            // Escreve o objeto para um arquivo XML
            final ByteArrayOutputStream outputHeader = new ByteArrayOutputStream();
            final StreamResult resultHeader = new StreamResult(outputHeader);
            marshaller.marshal(metadataJAXB, resultHeader);

            // Cria o cabeçalho
            final SoapMessage soapMessage = (SoapMessage) message;

            soapMessage.getEnvelope().addNamespaceDeclaration("soap",
                    soapMessage.getEnvelope().getName().getNamespaceURI());
            final SoapHeader header = soapMessage.getSoapHeader();

            // Transforma os dados em arquivo XML
            final StringSource headerSource = new StringSource(outputHeader.toString());
            transform(headerSource, header.getResult());

            /**
             * Adicionando ao soapHeader a parte de seguranca
             */
            WSSecSAMLToken wsSign = new WSSecSAMLToken();

            Document doc = soapMessage.getDocument();
            WSSecHeader secHeader = new WSSecHeader();
            secHeader.insertSecurityHeader(doc);

            wsSign.build(doc, assertionWrapper, secHeader);

        } catch (Exception e) {
            throw new TransformerException("Erro ao configurar servico de autenticacao");
        }

    }

    private JAXBElement<String> criarElementoJAXB(String chave, String valor) {
        return new JAXBElement<>(new QName(chave), String.class, valor);
    }

    private JAXBElement<AbstractEnumComplexType> criarElementoJAXB(String chave, AbstractEnumComplexType valor) {
        return new JAXBElement<>(new QName(chave), AbstractEnumComplexType.class, valor);
    }
}
