package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.xml.AbstractXMLObjectBuilder;
import org.opensaml.xml.signature.XMLSignatureBuilder;
import org.opensaml.xml.util.XMLConstants;

public class KeyAliasCustomBuilder extends AbstractXMLObjectBuilder<KeyAliasCustom>
        implements XMLSignatureBuilder<KeyAliasCustom> {

    /*
     * Constructor
     *
     */
    public KeyAliasCustomBuilder() {
    }

    /** {@inheritDoc} */
    public KeyAliasCustom buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new KeyAliasCustomImpl(namespaceURI, localName, namespacePrefix);
    }

    /** {@inheritDoc} */
    public KeyAliasCustom buildObject() {
        return buildObject(XMLConstants.XMLSIG_NS, KeyAliasCustom.DEFAULT_ELEMENT_LOCAL_NAME,
                XMLConstants.XMLSIG_PREFIX);
    }

}
