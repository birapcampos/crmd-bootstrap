package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import javax.xml.namespace.QName;

import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.util.XMLConstants;

public interface KeyValueCustom extends XSString {

    /** Element local name */
    public final static String DEFAULT_ELEMENT_LOCAL_NAME = "KeyValue";

    /** Default element name */
    public final static QName DEFAULT_ELEMENT_NAME = new QName(XMLConstants.XMLSIG_NS, DEFAULT_ELEMENT_LOCAL_NAME,
            XMLConstants.XMLSIG_PREFIX);

}
