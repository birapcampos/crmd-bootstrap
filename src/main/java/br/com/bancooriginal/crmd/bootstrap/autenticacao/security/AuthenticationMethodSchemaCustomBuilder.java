package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.common.impl.AbstractSAMLObjectBuilder;
import org.opensaml.common.xml.SAMLConstants;

public class AuthenticationMethodSchemaCustomBuilder
        extends AbstractSAMLObjectBuilder<AuthenticationMethodSchemaCustom> {

    /*
     * Constructor
     *
     */
    public AuthenticationMethodSchemaCustomBuilder() {
    }

    /** {@inheritDoc} */
    public AuthenticationMethodSchemaCustom buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new AuthenticationMethodSchemaCustomImpl(namespaceURI, localName, namespacePrefix);
    }

    /** {@inheritDoc} */
    public AuthenticationMethodSchemaCustom buildObject() {
        return buildObject(SAMLConstants.SAML20_NS, AuthenticationMethodSchemaCustomImpl.DEFAULT_ELEMENT_LOCAL_NAME,
                SAMLConstants.SAML20_PREFIX);
    }

}
