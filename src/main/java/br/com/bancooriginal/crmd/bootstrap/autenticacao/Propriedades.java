package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import br.com.bancooriginal.crmd.bootstrap.exception.ArquivoPropriedadesException;

/**
 * 
 * @author mvieira
 *
 */
public class Propriedades {

	/**
	 * O caminho do arquivo de propriedades que sera recuperado para o preenchimento
	 * dos valores.
	 */
	ResourceBundle bundle = ResourceBundle.getBundle("application");
	final String mbio = bundle.getString("arquivo.properties.interno");

	private final Properties properties;

	/**
	 * Construtor padrao que inicializa o arquivo de propriedades responsavel por
	 * preencher os valors do <b>Header</b> dos <tt>XML Request</tt>
	 */
	public Propriedades() {
		this.properties = new Properties();
		setMBIOPropriedades(mbio);
	}

	/**
	 * Carrega o arquivo de propriedades no construtor.
	 * 
	 * @param arquivoMapeamento arquivo de propriedades, apenas o nome do arquivo.
	 */
	public Propriedades(String arquivoMapeamento) {
		this.properties = new Properties();

		try (InputStream inputStream = this.getClass().getResourceAsStream("/" + arquivoMapeamento)) {
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			throw new ArquivoPropriedadesException(e);
		} catch (IOException e) {
			throw new ArquivoPropriedadesException(e);
		}
	}

	/**
	 * Este metodo ira recuperar apenas arquivos de propriedades externalizados.
	 * 
	 * @param caminhoArquivoMBIO caminho do arquivo de propriedades.
	 */
	private final void setMBIOPropriedades(final String caminhoArquivoMBIO) {

		try (FileInputStream input = new FileInputStream(caminhoArquivoMBIO)) {
			properties.load(input);
		} catch (FileNotFoundException e) {
			throw new ArquivoPropriedadesException(e);
		} catch (IOException e) {
			throw new ArquivoPropriedadesException(e);
		}
	}

	/**
	 * @return propriedades.
	 */
	public Set<String> chaves() {
		return properties.stringPropertyNames();
	}

	/**
	 * Atraves da chave o metodo retornara o valor.
	 * 
	 * @param chave chave para recuperar o valor
	 * @return o valor da chave
	 */
	public String valor(final String chave) {
		return properties.getProperty(chave);
	}

}
