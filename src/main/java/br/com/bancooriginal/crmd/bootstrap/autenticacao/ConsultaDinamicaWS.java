package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import net.technisys.cmm.services.singleselectdynamicauthenticationbyalias.rq.v1.Customer;
import net.technisys.cmm.services.singleselectdynamicauthenticationbyalias.rq.v1.ObjectFactory;
import net.technisys.cmm.services.singleselectdynamicauthenticationbyalias.rq.v1.SingleSelectDynamicAuthenticationByAliasRequest;
import net.technisys.cmm.services.singleselectdynamicauthenticationbyalias.rs.v1.SingleSelectDynamicAuthenticationByAliasResponse;

/**
 * @author rbnascimento
 *
 */
@Service
public class ConsultaDinamicaWS extends WebServiceTemplate {

    private static Jaxb2Marshaller marshaller;
    private Propriedades propriedade = new Propriedades();
    /**
     * Servico SOAP.
     */
    public static final SaajSoapMessageFactory saajSoapMessageFactory;

    static {
        marshaller = new Jaxb2Marshaller();
        saajSoapMessageFactory = serviceSoap();

    }

    public ConsultaDinamicaWS() {
        super(saajSoapMessageFactory);
        String[] targetNameSpaces = { propriedade.valor("marshaller.consulta.dinamica.package.scan.rq"),
                propriedade.valor("marshaller.consulta.dinamica.package.scan.rs") };
        marshaller.setPackagesToScan(targetNameSpaces);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
        this.setDefaultUri(propriedade.valor("pulbo.ws.cmmo.endpoint"));
        this.setCheckConnectionForFault(false);
    }

    /**
     * Adiciona AuthenticationMethodSchema e AuthenticationType ao
     * autenticacaoLdapDTO
     * 
     * @param autenticacaoLdap
     */
    @Cacheable("ConsultaDinamicaServiceCache")
    public void getResponse(AutenticacaoLdap autenticacaoLdap) throws SoapFaultClientException, Exception {

        ObjectFactory objFactory = new ObjectFactory();
        SingleSelectDynamicAuthenticationByAliasRequest request = objFactory
                .createSingleSelectDynamicAuthenticationByAliasRequest();

        Customer customer = objFactory.createCustomer();
        customer.setName("customer");
        customer.setAlias(autenticacaoLdap.getUsername());

        request.setCustomer(customer);

        JAXBElement<SingleSelectDynamicAuthenticationByAliasRequest> req = objFactory
                .createSingleSelectDynamicAuthenticationByAliasRequest(request);

        try {

            @SuppressWarnings("unchecked")
            JAXBElement<SingleSelectDynamicAuthenticationByAliasResponse> response = (JAXBElement<SingleSelectDynamicAuthenticationByAliasResponse>) this
                    .marshalSendAndReceive(propriedade.valor("ws.cmmo.endpoint"), req,
                            new CallBackSecurity("singleSelectDynamicAuthenticationByAlias", "PCR",
                                    autenticacaoLdap));

            autenticacaoLdap.setAuthenticationType(response.getValue().getAuthentication().getDynamicAuthenticacion()
                    .getAuthenticationType().getAuthenticationTypeId());
            autenticacaoLdap.setAuthenticationMethodSchema(
                    response.getValue().getAuthentication().getAuthenticationSchema().getAuthenticationSchemaId());

        } catch (SoapFaultClientException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    private static SaajSoapMessageFactory serviceSoap() {
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory();
        saajSoapMessageFactory.setSoapVersion(SoapVersion.SOAP_12);
        saajSoapMessageFactory.afterPropertiesSet();

        return saajSoapMessageFactory;
    }
}
