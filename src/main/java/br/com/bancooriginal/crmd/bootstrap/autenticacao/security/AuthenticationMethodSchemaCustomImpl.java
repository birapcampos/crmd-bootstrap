package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.xml.schema.impl.XSStringImpl;

public class AuthenticationMethodSchemaCustomImpl extends XSStringImpl implements AuthenticationMethodSchemaCustom {

    protected AuthenticationMethodSchemaCustomImpl(String namespaceURI,
            String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
        super.setSchemaType(TYPE_NAME);
    }

}
