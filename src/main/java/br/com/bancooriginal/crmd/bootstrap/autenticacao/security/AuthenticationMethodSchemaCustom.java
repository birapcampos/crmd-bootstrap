package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import javax.xml.namespace.QName;

import org.opensaml.common.SAMLObject;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.xml.schema.XSString;

public interface AuthenticationMethodSchemaCustom extends SAMLObject, XSString {

    /** Element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "authenticationMethodSchema";

    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(SAMLConstants.SAML20_NS, DEFAULT_ELEMENT_LOCAL_NAME,
            SAMLConstants.SAML20_PREFIX);

}
