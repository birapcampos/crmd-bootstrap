package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import net.technisys.cmm.services.verifysystemuserprofiling.rq.v1.ObjectFactory;
import net.technisys.cmm.services.verifysystemuserprofiling.rq.v1.VerifySystemUserProfilingRequestType;
import net.technisys.cmm.services.verifysystemuserprofiling.rs.v1.VerifySystemUserProfilingResponseType;

/**
 * @author rbnascimento
 *
 */
@Service
public class ValidaPerfilWS extends WebServiceTemplate {

    private static Jaxb2Marshaller marshaller;
    private Propriedades propriedade = new Propriedades();
    /**
     * Servico SOAP.
     */
    public static final SaajSoapMessageFactory saajSoapMessageFactory;

    static {
        saajSoapMessageFactory = serviceSoap();
        marshaller = new Jaxb2Marshaller();
    }

    public ValidaPerfilWS() {
        super(saajSoapMessageFactory);
        String[] targetNameSpaces = { propriedade.valor("marshaller.valida.perfil.package.scan.rq"),
                propriedade.valor("marshaller.valida.perfil.package.scan.rs") };
        marshaller.setPackagesToScan(targetNameSpaces);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
        this.setDefaultUri(propriedade.valor("ws.cmmo.endpoint"));
        this.setCheckConnectionForFault(false);
    }

    @Cacheable("ValidaPerfilServiceCache")
    public VerifySystemUserProfilingResponseType getUserID(AutenticacaoLdap autenticacaoLdap)
            throws SoapFaultClientException, Exception {

        ObjectFactory objFactory = new ObjectFactory();
        JAXBElement<VerifySystemUserProfilingRequestType> request = objFactory
                .createVerifySystemUserProfilingRequest(new VerifySystemUserProfilingRequestType());

        try {

            @SuppressWarnings("unchecked")
            JAXBElement<VerifySystemUserProfilingResponseType> response = (JAXBElement<VerifySystemUserProfilingResponseType>) this
                    .marshalSendAndReceive(propriedade.valor("ws.cmmo.endpoint"), request,
                            new CallBackSecurityWss("verifySystemUserProfiling", "PCR", autenticacaoLdap));

            return response.getValue();

        } catch (SoapFaultClientException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    private static SaajSoapMessageFactory serviceSoap() {
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory();
        saajSoapMessageFactory.setSoapVersion(SoapVersion.SOAP_12);
        saajSoapMessageFactory.afterPropertiesSet();

        return saajSoapMessageFactory;
    }

}
