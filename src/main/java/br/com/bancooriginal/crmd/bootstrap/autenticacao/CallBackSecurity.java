package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;
import org.springframework.xml.transform.TransformerObjectSupport;

import net.technisys.cmm.services.metadata.v2.AbstractEnumComplexType;
import net.technisys.cmm.services.metadata.v2.Metadata;
import net.technisys.cmm.services.metadata.v2.ObjectFactory;

/**
 *
 * @author rbnascimento
 *
 */
public class CallBackSecurity extends TransformerObjectSupport implements WebServiceMessageCallback {

    private AutenticacaoLdap autenticacao;
    private String serviceId;
    private String system;
    private final Propriedades propriedade = new Propriedades();

    /**
     * Construtor
     */
    public CallBackSecurity(String serviceId, String system, AutenticacaoLdap autenticacao) {
        super();
        this.autenticacao = autenticacao;
        this.serviceId = serviceId;
        this.system = system;
    }

    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
        final ObjectFactory objectFactory = new ObjectFactory();
        /**
         * Campos Parametros
         */
        AbstractEnumComplexType channel = new AbstractEnumComplexType();
        channel.setMnemonic(system);

        String userId = autenticacao.getUserID() != null ? autenticacao.getUserID()
                : autenticacao.getExecutingOperatorId();

        Metadata metadata = objectFactory.createMetadata();
        metadata.setServiceId(serviceId);
        metadata.setSessionId(autenticacao.getSessionID());
        JAXBElement<String> userIdJAXBElement = this.criarElementoJAXB("executingOperatorId", userId);
        metadata.setExecutingOperatorId(userIdJAXBElement);
        metadata.setAddress(autenticacao.getIp());

        /**
         * Fixos
         */
        metadata.setServiceVersion("1.0");
        JAXBElement<AbstractEnumComplexType> channelJAXBElement = this.criarElementoJAXB("executingChannel",
                channel);
        metadata.setExecutingChannel(channelJAXBElement);
        JAXBElement<String> institutionIDJAXBElement = this.criarElementoJAXB("institutionId", "1111");
        metadata.setInstitutionId(institutionIDJAXBElement);
        JAXBElement<String> organizationIDJAXBElement = this.criarElementoJAXB("organizationId", "1111");
        metadata.setOrganizationId(organizationIDJAXBElement);
        JAXBElement<String> branchIDJAXBElement = this.criarElementoJAXB("branchId", "1");
        metadata.setBranchId(branchIDJAXBElement);
        metadata.setLocale("pt_BR");
        JAXBElement<String> terminalIDJAXBElement = this.criarElementoJAXB("terminalId", "ws");
        metadata.setTerminalId(terminalIDJAXBElement);

        JAXBElement<Metadata> metadataJAXB = objectFactory.createMetadata(metadata);

        // Seta o marshaller
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan(propriedade.valor("marshaller.metadata.package.scan"));

        // Escreve o objeto para um arquivo XML
        final ByteArrayOutputStream outputHeader = new ByteArrayOutputStream();
        final StreamResult resultHeader = new StreamResult(outputHeader);
        marshaller.marshal(metadataJAXB, resultHeader);

        // Cria o cabeçalho
        final SoapMessage soapMessage = (SoapMessage) message;

        soapMessage.getEnvelope().addNamespaceDeclaration("soap",
                soapMessage.getEnvelope().getName().getNamespaceURI());
        final SoapHeader header = soapMessage.getSoapHeader();

        // Transforma os dados em arquivo XML
        final StringSource headerSource = new StringSource(outputHeader.toString());
        transform(headerSource, header.getResult());
    }

    private JAXBElement<String> criarElementoJAXB(String chave, String valor) {
        return new JAXBElement<>(new QName(chave), String.class, valor);
    }

    private JAXBElement<AbstractEnumComplexType> criarElementoJAXB(String chave, AbstractEnumComplexType valor) {
        return new JAXBElement<>(new QName(chave), AbstractEnumComplexType.class, valor);
    }
}
