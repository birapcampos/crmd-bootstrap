package br.com.bancooriginal.crmd.bootstrap.autenticacao;

/**
 * @author rbnascimento
 * 
 */
public enum Roles {
   BIM_PRM_VIG,
   BIM_SVE_PRM,
   BIM_CST_APD,
   BIM_PVG_CTA,
   BIM_ALT_MNR,
   GESTAOCOMERCIAL_BIM
}
