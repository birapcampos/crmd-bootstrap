package br.com.bancooriginal.crmd.bootstrap.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

import br.com.bancooriginal.crmd.bootstrap.utils.crmdBootStraoLogger;

public class ArquivoPropriedadesException extends RuntimeException {

	private static final long serialVersionUID = 662192786855579587L;
	private static final crmdBootStraoLogger LOGGER = new crmdBootStraoLogger();

	public ArquivoPropriedadesException(FileNotFoundException e) {
		super("Arquivo de propriedades externo não encontrado", e);
		LOGGER.error("Arquivo de propriedades externo não encontrado", e);
	}

	public ArquivoPropriedadesException(IOException e) {
		super("Erro ao ler Arquivo de propriedades", e);
		LOGGER.error("Erro ao ler Arquivo de propriedades", e);
	}
}