package br.com.bancooriginal.crmd.bootstrap.exception;

public class crmdBootStrapException extends Exception {
	
	private static final long serialVersionUID = -6088596271293474069L;

	public crmdBootStrapException(String message, Throwable cause){
		super(message, cause);
	}
	
	public crmdBootStrapException(String message){
		super(message);
	}
	
	public crmdBootStrapException(Throwable cause){
		super(cause.getMessage(), cause);
	}		

}
