package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.xml.schema.impl.XSStringImpl;

public class KeyAliasCustomImpl extends XSStringImpl implements KeyAliasCustom {

    /**
     * Constructor
     *
     * @param namespaceURI
     * @param elementLocalName
     * @param namespacePrefix
     */
    protected KeyAliasCustomImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
        super.setSchemaType(TYPE_NAME);
    }

}
