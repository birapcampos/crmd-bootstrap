package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.xml.AbstractXMLObjectBuilder;
import org.opensaml.xml.signature.XMLSignatureBuilder;
import org.opensaml.xml.util.XMLConstants;

public class KeyValueCustomBuilder extends AbstractXMLObjectBuilder<KeyValueCustom>
        implements XMLSignatureBuilder<KeyValueCustom> {

    /*
     * Constructor
     *
     */
    public KeyValueCustomBuilder() {
    }

    /** {@inheritDoc} */
    public KeyValueCustom buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new KeyValueCustomImpl(namespaceURI, localName, namespacePrefix);
    }

    /** {@inheritDoc} */
    public KeyValueCustom buildObject() {
        return buildObject(XMLConstants.XMLSIG_NS, KeyValueCustom.DEFAULT_ELEMENT_LOCAL_NAME,
                XMLConstants.XMLSIG_PREFIX);
    }

}
