package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import net.technisys.cmm.services.singleselectsystemuserfunctionalities.rq.v1.ObjectFactory;
import net.technisys.cmm.services.singleselectsystemuserfunctionalities.rq.v1.SingleSelectSystemUserFunctionalitiesRequest;
import net.technisys.cmm.services.singleselectsystemuserfunctionalities.rs.v1.SecurityItemType;
import net.technisys.cmm.services.singleselectsystemuserfunctionalities.rs.v1.SingleSelectSystemUserFunctionalitiesResponseType;


@Service
public class RolesUsuarioWS extends WebServiceTemplate {

    private static Jaxb2Marshaller marshaller;
    private Propriedades propriedade = new Propriedades();
    /**
     * Servico SOAP.
     */
    public static final SaajSoapMessageFactory saajSoapMessageFactory;

    static {
        saajSoapMessageFactory = serviceSoap();
        marshaller = new Jaxb2Marshaller();
    }

    public RolesUsuarioWS() {
        super(saajSoapMessageFactory);
        String[] targetNameSpaces = { propriedade.valor("marshaller.roles.package.scan.rq"),
                propriedade.valor("marshaller.roles.package.scan.rs") };
        marshaller.setPackagesToScan(targetNameSpaces);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
        this.setDefaultUri(propriedade.valor("pulbo.ws.cmmo.endpoint"));
        this.setCheckConnectionForFault(false);
    }

    @Cacheable("RolesServiceCache")
    public List<String> getFuncionalidades(AutenticacaoLdap autenticacaoLdap)
            throws SoapFaultClientException, Exception {

        ObjectFactory objFactory = new ObjectFactory();
        SingleSelectSystemUserFunctionalitiesRequest request = objFactory
                .createSingleSelectSystemUserFunctionalitiesRequest();

        try {

            @SuppressWarnings("unchecked")
            JAXBElement<SingleSelectSystemUserFunctionalitiesResponseType> response = (JAXBElement<SingleSelectSystemUserFunctionalitiesResponseType>) this
                    .marshalSendAndReceive(propriedade.valor("ws.cmmo.endpoint"), request,
                            new CallBackSecurity("singleSelectSystemUserFunctionalities", "PCR", autenticacaoLdap));

            List<String> listRoles = new ArrayList<String>();

            for (SecurityItemType securityItem : response.getValue().getSystemUser().getSecurityItems()
                    .getSecurityItem()) {
                listRoles.add(securityItem.getSecurityItemAlias());
            }

            return listRoles;

        } catch (SoapFaultClientException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    private static SaajSoapMessageFactory serviceSoap() {
        SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory();
        saajSoapMessageFactory.setSoapVersion(SoapVersion.SOAP_12);
        saajSoapMessageFactory.afterPropertiesSet();

        return saajSoapMessageFactory;
    }

}
