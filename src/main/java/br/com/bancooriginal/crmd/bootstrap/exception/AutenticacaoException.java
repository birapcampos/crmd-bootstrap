package br.com.bancooriginal.crmd.bootstrap.exception;

public class AutenticacaoException extends Exception {

	private static final long serialVersionUID = -1099677564336624771L;

	public AutenticacaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public AutenticacaoException(String message) {
		super(message);
	}

	public AutenticacaoException(Throwable cause) {
		super(cause.getMessage(), cause);
	}

}
