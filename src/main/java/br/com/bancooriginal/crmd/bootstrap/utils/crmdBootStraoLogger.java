package br.com.bancooriginal.crmd.bootstrap.utils;

import org.slf4j.LoggerFactory;

/**
 * Wrapper utilizado para realizar as chamadas de log do sistema.
 */
public class crmdBootStraoLogger{

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger("BIMO-logger");

    /**
     * Gera mensagens de log com nivel debug
     *
     * @param identificacaoAcao
     *            Identificação da acao
     * @param mensagem
     *            Mensagem do log
     */
    public void debug(String identificacaoAcao, String mensagem) {
        LOGGER.debug(this.criarLog(identificacaoAcao, mensagem));
    }

    /**
     * Gera mensagens de log com nivel info
     *
     * @param identificacaoAcao
     *            Identificação da acao
     * @param mensagem
     *            Mensagem do log
     */
    public void info(String identificacaoAcao, String mensagem) {
        LOGGER.info(this.criarLog(identificacaoAcao, mensagem));
    }

    /**
     * Gera mensagens de log com nivel error
     *
     * @param identificacaoAcao
     *            Identificação da acao
     * @param erro
     *            Erro ocorrido
     */
    public void error(String identificacaoAcao, Exception erro) {
        LOGGER.error(this.criarLog(identificacaoAcao, erro.getMessage()), erro);
    }

    /**
     * Gera mensagens de log com nivel warn
     *
     * @param identificacaoAcao Identificação da ação
     * @param ex Erro ocorrido.
     */
    public void warn(String identificacaoAcao, Exception ex) {
        LOGGER.warn(this.criarLog(identificacaoAcao, ex.getMessage()), ex);
    }


    private String criarLog(String identificacaoAcao, String mensagem) {
        return new BIMOLog(identificacaoAcao, mensagem).toString();
    }


    private static class BIMOLog {
        private final String identificacaoAcao;
        private final String mensagem;

        private BIMOLog(String identificacaoAcao, String mensagem) {
            this.identificacaoAcao = identificacaoAcao;
            this.mensagem = mensagem;
        }

        @Override
        public String toString() {
            return identificacaoAcao + " - " + mensagem;
        }
    }
}
