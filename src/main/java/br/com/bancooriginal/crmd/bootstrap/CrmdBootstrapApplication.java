package br.com.bancooriginal.crmd.bootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmdBootstrapApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmdBootstrapApplication.class, args);
	}

}
