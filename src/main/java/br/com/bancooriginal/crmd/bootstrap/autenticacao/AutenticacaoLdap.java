package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.io.Serializable;

public class AutenticacaoLdap implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4399217572363526088L;
    private static final String URI_PREFIX = "uriTech=";
    private static final String CHANNEL_PREFIX = "PCR_";

    private String sessionID;
    private String ip;
    private String username;
    private String password;
    private String authenticationMethodSchema;
    private String userID;
    private String authenticationType;

    public AutenticacaoLdap(String sessionID) {
        this.sessionID = sessionID;
    }

    public AutenticacaoLdap(String sessionID, String ip) {
        this.sessionID = sessionID;
        this.ip = ip;
    }

    public String getSessionID() {
        return CHANNEL_PREFIX + sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getIp() {
        return ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAuthenticationMethodSchema() {
        return authenticationMethodSchema;
    }

    public void setAuthenticationMethodSchema(String authenticationMethodSchema) {
        this.authenticationMethodSchema = authenticationMethodSchema;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public String getExecutingOperatorId() {
        return URI_PREFIX + this.username;
    }

    @Override
    public String toString() {
        return "AutenticacaoLdapDTO [sessionID=" + sessionID
                + ", ip=" + ip
                + ", username=" + username
                + ", password=" + password
                + ", authenticationMethodSchema=" + authenticationMethodSchema
                + ", userID=" + userID
                + ", authenticationType=" + authenticationType + "]";
    }

}
