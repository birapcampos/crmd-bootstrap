package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;

/**
 * User Metadata, used to build the LTPA Token
 */
public class UserMetadata implements Serializable {

	private static final long serialVersionUID = -6000428072455633546L;
	private long expire = 0l;
	private String user = null;

	private String idLogin = "uid=";
	private String fimLogin = "&&";

	private String idNome = "CN=";
	private String fimNome = ",OU=";

	private byte[] signature = null;
	private TokenLTPAFactory.LTPA_VERSION ltpaVersion = TokenLTPAFactory.LTPA_VERSION.LTPA;

	private static final Base64 base64 = new Base64();

	public UserMetadata() {
		super();
	}

	/**
	 * Basic constructor
	 * 
	 * @param plainToken
	 * @param ltpaVersion
	 * @throws Exception
	 */
	public UserMetadata(String plainToken, TokenLTPAFactory.LTPA_VERSION ltpaVersion) throws Exception {
		this.ltpaVersion = ltpaVersion;

		String[] parts = plainToken.split("\\%", 2);
		this.expire = Long.parseLong(parts[1].split("\\%")[0]);
		String[] tokens = parts[0].split("\\$");

		for (int i = 0; i < tokens.length; ++i) {
			String[] nameValue = tokens[i].split(":", 2);
			if ("expire".equals(nameValue[0])) {
				this.expire = Long.parseLong(nameValue[1]);
			} else if ("u".equals(nameValue[0])) {
				this.user = nameValue[1];
			}
		}
		this.signature = base64.decode(plainToken.split("%")[1]);
	}

	public String getNome() {
		String nome = this.user;
		// Mostramos o valor do CN que seria o Nome
		nome = nome.substring(nome.indexOf(idNome) + idNome.length(), nome.indexOf(fimNome));
		return nome;
	}

	public String getLogin() {
		String login = this.user;
		// Mostramos o valor do uid que seria o Login
		login = login.substring(login.indexOf(idLogin) + idLogin.length(), login.indexOf(fimLogin));
		return login;
	}

	/**
	 * Encode the user data to a plain LTPA token string
	 * 
	 * @return
	 */
	public String getPlainUserMetadata() {
		StringBuilder str = new StringBuilder();

		if (ltpaVersion.equals(TokenLTPAFactory.LTPA_VERSION.LTPA2)) {
			str.append("expire:").append(this.expire).append("$");
		}
		str.append("u:").append(this.user);

		return str.toString();
	}

	public String getPlainUserAndExpire() {
		StringBuilder str = new StringBuilder();

		if (ltpaVersion.equals(TokenLTPAFactory.LTPA_VERSION.LTPA2)) {
			str.append("expire:").append(this.expire).append("$");
		}
		str.append("u:").append(this.user).append("%").append(this.expire).append("%");

		return str.toString();
	}

	public String toString() {
		StringBuilder str = new StringBuilder(getPlainUserMetadata());
		str.append("%").append(this.expire).append("%");
		str.append(base64.encodeToString(this.signature).replaceAll("[\r\n]", ""));
		return str.toString();
	}

	public long getExpire() {
		return expire;
	}

	public void setExpire(long expire) {
		this.expire = expire;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public byte[] getSignature() {
		return signature.clone();
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

	public TokenLTPAFactory.LTPA_VERSION getLtpaVersion() {
		return ltpaVersion;
	}

	public void setLtpaVersion(TokenLTPAFactory.LTPA_VERSION ltpaVersion) {
		this.ltpaVersion = ltpaVersion;
	}
}
