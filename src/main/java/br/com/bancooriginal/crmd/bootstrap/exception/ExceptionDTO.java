package br.com.bancooriginal.crmd.bootstrap.exception;

public class ExceptionDTO {

	private String errorMessage;

	public ExceptionDTO() {
		super();
	}

	public ExceptionDTO(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
