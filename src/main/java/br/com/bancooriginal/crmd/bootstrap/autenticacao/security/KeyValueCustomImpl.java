package br.com.bancooriginal.crmd.bootstrap.autenticacao.security;

import org.opensaml.xml.schema.impl.XSStringImpl;

public class KeyValueCustomImpl extends XSStringImpl implements KeyValueCustom {

    /**
     * Constructor
     *
     * @param namespaceURI
     * @param elementLocalName
     * @param namespacePrefix
     */
    protected KeyValueCustomImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
        super.setSchemaType(TYPE_NAME);
    }

}
