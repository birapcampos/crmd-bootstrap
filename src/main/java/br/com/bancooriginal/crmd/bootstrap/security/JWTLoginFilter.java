package br.com.bancooriginal.crmd.bootstrap.security;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bancooriginal.crmd.bootstrap.utils.crmdBootStraoLogger;


public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

	private static crmdBootStraoLogger LOG = new crmdBootStraoLogger();
	
	public JWTLoginFilter(String url, AuthenticationManager authManager) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException, IOException, ServletException {
		
		LOG.info("Segurança", "Iniciando checagem de autorização do usuário de serviço.");
		
		UserCredentials creds = new ObjectMapper().readValue(req.getInputStream(), UserCredentials.class);
		LOG.debug("Segurança", "Credenciais: " + creds.toString());
		return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(creds.getUsername(),
				creds.getPassword(), Collections.<GrantedAuthority>emptyList()));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		
		LOG.info("Segurança", "Acesso autorizado.");
		
		TokenAuthenticationService.addAuthentication(res, auth.getName());
	}
	
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		
		LOG.error("Segurança", failed);
		
		super.unsuccessfulAuthentication(request, response, failed);
	}

}
