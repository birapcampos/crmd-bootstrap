package br.com.bancooriginal.crmd.bootstrap.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import br.com.bancooriginal.crmd.bootstrap.autenticacao.Propriedades;
import br.com.bancooriginal.crmd.bootstrap.utils.crmdBootStraoLogger;

@Component
public class CorsFilter implements Filter{

	private crmdBootStraoLogger LOG = new crmdBootStraoLogger();
	private Propriedades propriedade = new Propriedades();
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.info("Incluindo CORS Angular", "CorsFilter iniciado");
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
		 	HttpServletRequest request = (HttpServletRequest) req;
	        HttpServletResponse response = (HttpServletResponse) res;

	        String urlPermitida = propriedade.valor("url.origin.angular");
	        
	        //Colocar aqui o origin do Angular
	        response.setHeader("Access-Control-Allow-Origin", urlPermitida);
	        response.setHeader("Access-Control-Allow-Credentials", "true");

	        if (("OPTIONS".equals(request.getMethod()) || "GET".equals(request.getMethod())) && urlPermitida.equals(request.getHeader("Origin"))) {
	            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
	            response.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept");
	            response.setHeader("Access-Control-Max-Age", "3600");
	            
	            chain.doFilter(req, res);
	        } else {
	        	
	            chain.doFilter(req, res);
	        }
		
	}

	@Override
	public void destroy() {
	}

}
