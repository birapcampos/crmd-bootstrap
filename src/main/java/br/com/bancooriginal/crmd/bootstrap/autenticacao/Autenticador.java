package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import br.com.bancooriginal.crmd.bootstrap.exception.AutenticacaoException;
import br.com.bancooriginal.crmd.bootstrap.utils.crmdBootStraoLogger;
import net.technisys.cmm.services.verifysystemuserprofiling.rs.v1.VerifySystemUserProfilingResponseType;

//import net.technisys.cmm.services.verifysystemuserprofiling.rs.v1.VerifySystemUserProfilingResponseType;

public class Autenticador extends AbstractPreAuthenticatedProcessingFilter {

	private static final String VALIDACAO = "Validacao";
	public static final String COOKIE_PORTAL_TOKEN_NAME = "PortalOriginal";
	public static final String SESSAO_USUARIO = "usuarioSSO";
	public static final String SESSAO_USUARIO_TOKEN = "usuarioToken";
	private Propriedades propriedade = new Propriedades();
	private static crmdBootStraoLogger LOGGER = new crmdBootStraoLogger();

	private ValidaPerfilWS validaPerfil = new ValidaPerfilWS();
	private ConsultaDinamicaWS consultaDinamicaWS = new ConsultaDinamicaWS();
	private RolesUsuarioWS rolesUsuarioWS = new RolesUsuarioWS();
	
	private String keyPassword;
	private String sharedKey;
	private String token;

	@Autowired
	private LdapTemplate ldapTemplate;

	@Override
	public Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

		User userAuthenticationTokenError = null;

		try {

			LOGGER.info(VALIDACAO, "Inicio da validacao do usuario");
			UserMetadata user = recuperaDescriptgrafaToken(request);

			if (dataTokenExpirada(user)) {
				LOGGER.info(VALIDACAO, " Sessao do usuario expirou.");
				throw new AutenticacaoException("Sessao do usuario expirou.");
			}

			// Atribui o usuario do token na sessao
			request.getSession().setAttribute(SESSAO_USUARIO_TOKEN, user);

			// Chamando os servicos de autenticação
			//Agora é passado o currentTimeMillis pois o CMM necessita 
			//que cada requisição seja diferente dentro de dois minutos
			AutenticacaoLdap autenticacaoLdap = new AutenticacaoLdap(String.valueOf(System.currentTimeMillis()),
					request.getRemoteAddr());
			User userAuthenticationToken = authUserByToken(user.getNome(), autenticacaoLdap);

			LOGGER.info(VALIDACAO, " Atribuindo usuario na sessao.");
			// Atribui o usuario autenticado na sessao
			//request.getSession().setAttribute(SESSAO_USUARIO, userAuthenticationToken);

			LOGGER.info(VALIDACAO, " Via SingleSignOn (Token)");
			return userAuthenticationToken;

		} catch (Exception e) {
			LOGGER.error(VALIDACAO, e);
			// invalida sessao do usuario
			request.getSession().invalidate();
			return userAuthenticationTokenError;
		}
	}

	private Boolean dataTokenExpirada(UserMetadata user) {
		Date dataExpire = new Date(user.getExpire());
		// Valida se a data expirou
		return new Date().after(dataExpire);
	}

	private UserMetadata recuperaDescriptgrafaToken(HttpServletRequest request) throws Exception {

		LOGGER.info(VALIDACAO, " Recuperando o token do cookie.");

		String internalToken = null;
		List<Cookie> allCookies = new ArrayList<>();
		if (getToken() != null) {
			allCookies.add(new Cookie(COOKIE_PORTAL_TOKEN_NAME, getToken()));
		}
		if (request.getCookies() != null && request.getCookies().length > 0) {
			allCookies.addAll(Arrays.asList(request.getCookies()));
		}

		if (!allCookies.isEmpty()) {
			LOGGER.info(VALIDACAO, " Cookies Recuperados.");
			for (Cookie cookie : allCookies) {
				if (COOKIE_PORTAL_TOKEN_NAME.equals(cookie.getName())) {
					LOGGER.info(VALIDACAO, " Token Recuperado.");
					internalToken = cookie.getValue() + "=";
					break;
				}
			}
		} else {
			LOGGER.info(VALIDACAO, " Nao foi possivel recuperar os cookies.");
		}

		if (internalToken == null || "".equals(internalToken)) {
			throw new AutenticacaoException("Nao foi possivel recuperar o Token da Requisicao.");
		}

		// Descriptografando o token
		LOGGER.info(VALIDACAO, " Descriptografando o Token");
		TokenLTPAFactory factory = new TokenLTPAFactory(keyPassword, sharedKey);
		return factory.decodeLTPATokenUser(internalToken, TokenLTPAFactory.LTPA_VERSION.LTPA2);

	}

	private User authUserByToken(String userName, AutenticacaoLdap autenticacaoLdap) throws Exception {
		LOGGER.info(VALIDACAO, " Buscando usuario no LDAP: ");
		String userId = buscarAliasLdap(userName);
		if (userId == null) {
			LOGGER.error(VALIDACAO, new AuthenticationServiceException("Usuario não encontrado no LDAP"));
			throw new AuthenticationServiceException("Usuario não encontrado no LDAP.");
		}
		
		autenticacaoLdap.setUsername(userId);
		LOGGER.info("Validacao", "Buscando os metodos de autenticacao(singleSelectDynamicAuthenticationByAlias)");

		// Recupera os metodos de autenticação

		try {
			consultaDinamicaWS.getResponse(autenticacaoLdap);
		} catch (Exception e) {
			LOGGER.error("Validacao -  Erro na autenticacao do usuario", e);
			throw new AuthenticationServiceException(
					"Usuario não encontrado no servico singleSelectDynamicAuthenticationByAlias.");
		}

		LOGGER.info("Validacao", " Metodos de autenticacao - OK");

		LOGGER.info("Validacao", " Autenticando o usuario(verifySystemUserProfiling)");

		// Efetua a autenticacao
		VerifySystemUserProfilingResponseType userProfilingResponseType = validaPerfil.getUserID(autenticacaoLdap);
		try {
			autenticacaoLdap.setUserID(userProfilingResponseType.getCollection().getSystemUser().getUserId());
			String id = userProfilingResponseType.getCollection().getSystemUser().getUserId();
			autenticacaoLdap.setUserID(id);

		} catch (Exception e) {
			LOGGER.error("Validacao - Erro na autenticacao do usuario", e);
			throw new AuthenticationServiceException("Usuario não encontrado no servico verifySystemUserProfiling.");
		}

		// Remove o prefix "uriTech=" por que já será incluido pelo DTO
		// automaticamente
		autenticacaoLdap
				.setUsername(autenticacaoLdap.getUserID().substring(autenticacaoLdap.getUserID().indexOf("=") + 1));

		LOGGER.info("Validacao", "Autenticacao do usuario - OK");

		/**
		 * Recupera a ROLEs do usuario
		 */
		List<String> roles = new ArrayList<>();

		LOGGER.info("Validacao", "Buscando roles do usuario");
		roles.addAll(rolesUsuarioWS.getFuncionalidades(autenticacaoLdap));
		
		if (roles.isEmpty()) {
			LOGGER.error("Validacao - Erro na autenticacao do usuario", new AuthenticationServiceException(
					"Usuario não encontrado no servico singleSelectSystemUserFunctionalities."));
			throw new AuthenticationServiceException(
					"Usuario não encontrado no servico singleSelectSystemUserFunctionalities.");
		}
		
		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		for (String role : roles) {
			grantedAuths.add(new SimpleGrantedAuthority(role));
		}
		
		return new User(userId, "", grantedAuths);
	}

	/**
	 *
	 * @param nomeUsuario
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String buscarAliasLdap(String nomeUsuario) {
		LOGGER.info(VALIDACAO, " Via buscarAliasLdap ()");
		LdapContextSource c = new org.springframework.ldap.core.support.LdapContextSource();
		c.setUrl(propriedade.valor("ldap.url"));
		c.setBase(propriedade.valor("ldap.base"));
		c.setUserDn(propriedade.valor("ldap.user"));
		c.setPassword(propriedade.valor("ldap.password"));
		c.afterPropertiesSet();

		ldapTemplate = new LdapTemplate(c);
		ldapTemplate.setIgnorePartialResultException(true);

		try {
			LOGGER.info(VALIDACAO, " Criando o filtro.");
			AndFilter filter = new AndFilter();
			filter.and(new EqualsFilter("objectclass", "person"));
			filter.and(new EqualsFilter("cn", nomeUsuario));

			LOGGER.info(VALIDACAO, " Buscando no LDAP.");
			List<String> listaAlias = ldapTemplate.search("", filter.encode(), new AttributesMapper() {
				public Object mapFromAttributes(Attributes attrs) throws NamingException {
					return attrs.get("sAMAccountName").get();
				}
			});

			if (listaAlias != null && !listaAlias.isEmpty()) {
				LOGGER.info(VALIDACAO, " Via buscarAliasLdap - OK");
				return listaAlias.get(0);
			}

		} catch (Exception e) {
			LOGGER.error(VALIDACAO, e);
		}

		LOGGER.info(VALIDACAO, " Usuario não encontrado no LDAP.");
		return null;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		String[] credentials = new String[1];
		credentials[0] = "NA";
		return credentials;
	}

	public String getKeyPassword() {
		return keyPassword;
	}

	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword;
	}

	public String getSharedKey() {
		return sharedKey;
	}

	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}

	public static void main(String[] args) throws Exception {

		String t = "Jgs3xb/wwod9zkBAqMpSRJ/IXl0M0vQC/8aeXD6BF45QHaBu00Pmkx7avxGrr4qA7GHER1WQZkjjup1a0xrrHlqGepj7izBhguFbfHCubcPUvpzTqUON5Uy3OKDXHuNGNyuCyZnEM32/xONk4U8YF4sr1r2B2owxNfi2J3Dq6IQoIuCzVwxfbgZC1Forrw7wuqEe4J8DFU5b7NcYIeblydUcS2pAZPVlYCoGDRNLTbUbb7+4YEEBqkXyobJ1rgL44PMiWhj/jCQnizGbHknIklIa9dd37enmgrtCFnkpyuZ23/wsgTk0Bn+F0arPnkbnzyf7vRpbyeaTJfA26ibjj9XWsTyW9qpjBi+n8xbiOUyaov7qAgwVsKGB6ZqcRahZvz/qW2u8oxMMohNDsNHRqFIx60ULoK1BkHWj8uUd0Ao=";
		TokenLTPAFactory factory = new TokenLTPAFactory(null, null);
		UserMetadata user = factory.decodeLTPATokenUser(t, TokenLTPAFactory.LTPA_VERSION.LTPA2);
		LOGGER.debug("debug", user.getNome());

		LdapContextSource c = new org.springframework.ldap.core.support.LdapContextSource();
		c.setUrl("ldap://externo.ad:389");
		c.setBase("DC=externo,DC=ad");
		c.setUserDn("CN=UGCENDBIND,OU=Internal Users,DC=externo,DC=ad");
		c.setPassword("Eiu4Gelp");

		LdapTemplate temp = new LdapTemplate(c);
		temp.setIgnorePartialResultException(true);

		AndFilter filter = new AndFilter();
		filter.and(new EqualsFilter("objectclass", "person"));
		filter.and(new EqualsFilter("cn", user.getNome()));

		temp.search("", filter.encode(), new AttributesMapper<Object>() {
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs.get("sAMAccountName").get();
			}
		});
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
