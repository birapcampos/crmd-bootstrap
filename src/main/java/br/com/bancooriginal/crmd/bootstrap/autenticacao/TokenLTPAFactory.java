package br.com.bancooriginal.crmd.bootstrap.autenticacao;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

import br.com.bancooriginal.crmd.bootstrap.exception.AutenticacaoException;


/**
 * Utility class witch handles LTPA Tokens
 * 
 * @author rbnascimento
 */
public class TokenLTPAFactory {

	private final Base64 base64 = new Base64();
	private Propriedades propriedade = new Propriedades();

	private String sharedKey;
	private String keyPassword;

	/**
	 * LTPA Versions
	 */
	public enum LTPA_VERSION {
		LTPA, LTPA2
	}

	/**
	 * Types of cripting algorithms
	 */
	private enum CRIPTING_ALGORITHM {
		AES_DECRIPTING_ALGORITHM("AES/CBC/PKCS5Padding"), DES_DECRIPTING_ALGORITHM("DESede/ECB/PKCS5Padding");

		private final String text;

		private CRIPTING_ALGORITHM(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return this.text;
		}
	}

	public TokenLTPAFactory(String keyPassword, String sharedKey) throws AutenticacaoException {
		this.keyPassword = keyPassword = propriedade.valor("com.ibm.websphere.ltpa.KeyPassword");
		this.sharedKey = sharedKey = propriedade.valor("com.ibm.websphere.ltpa.3DESKey");
		if (this.keyPassword == null) {
			throw new AutenticacaoException("Invalid Key Password");
		}
	}

	/**
	 * Prepare an initialization vector for the cryptografic algorithm
	 * 
	 * @param key  of the crypt algorithm
	 * @param size of the vector
	 * @return
	 */
	private IvParameterSpec generateIvParameterSpec(byte[] key, int size) {
		byte[] row = new byte[size];
		for (int i = 0; i < size; i++) {
			row[i] = key[i];
		}
		return new IvParameterSpec(row);
	}

	/**
	 * Helper function which do the hard work of encrypting and decrypting
	 * 
	 * @param target    data to be [en/de]crypted
	 * @param key       DES key
	 * @param algorithm Algorithm used during the crypting process
	 * @param mode      1 for decrypting and 2 for encrypting
	 * @return dados [en/de]crypted data
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidAlgorithmParameterException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws Exception
	 */
	private byte[] crypt(byte[] target, byte[] key, CRIPTING_ALGORITHM algorithm, int mode)
			throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		SecretKey sKey = null;

		if (algorithm.name().indexOf("AES") != -1) {
			sKey = new SecretKeySpec(key, 0, 16, "AES");
		} else {
			DESedeKeySpec kSpec = new DESedeKeySpec(key);
			SecretKeyFactory kFact = SecretKeyFactory.getInstance("DESede");
			sKey = kFact.generateSecret(kSpec);
		} // else
		Cipher cipher = Cipher.getInstance(algorithm.text);

		if (algorithm.name().indexOf("ECB") == -1) {
			if (algorithm.name().indexOf("AES") != -1) {
				IvParameterSpec ivs16 = generateIvParameterSpec(key, 16);
				cipher.init(mode, sKey, ivs16);
			} else {
				cipher.init(mode, sKey);
			} // else
		} else {
			cipher.init(mode, sKey);
		} // else
		return cipher.doFinal(target);
	}

	/**
	 * Decrypt a given encoded key, using a DES algorithm
	 * 
	 * @param key
	 * @param keyPassword
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws InvalidKeySpecException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws Exception
	 */
	private byte[] getSecretKey(String key, String keyPassword) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(keyPassword.getBytes());
		byte[] hash3DES = new byte[24];
		System.arraycopy(md.digest(), 0, hash3DES, 0, 20);
		Arrays.fill(hash3DES, 20, 24, (byte) 0);
		final Cipher cipher = Cipher.getInstance(CRIPTING_ALGORITHM.DES_DECRIPTING_ALGORITHM.text);
		final KeySpec keySpec = new DESedeKeySpec(hash3DES);
		final Key secretKey = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return cipher.doFinal(base64.decode(key));
	}

	/**
	 * Decode a LTPA v1 or v2 token
	 * 
	 * @param tokenLTPA
	 * @param version
	 * @return
	 * @throws Exception
	 */
	public String decodeLTPAToken(String tokenLTPA, LTPA_VERSION version) throws Exception {
		// lets get the shared key
		byte[] sKey = getSecretKey(this.sharedKey, this.keyPassword);
		// and decode from base64 to bytes the given token
		byte[] encryptedBytes = base64.decode(tokenLTPA);
		// to get the plain decrypted token after applying the decrypting
		// algorithm
		String plainToken = new String(crypt(encryptedBytes, sKey,
				version.equals(LTPA_VERSION.LTPA2) ? CRIPTING_ALGORITHM.AES_DECRIPTING_ALGORITHM
						: CRIPTING_ALGORITHM.DES_DECRIPTING_ALGORITHM,
				Cipher.DECRYPT_MODE), StandardCharsets.UTF_8);
		// finally, lets parse the decrypted token into the user metadata
		UserMetadata user = new UserMetadata(plainToken, version);
		return user.getPlainUserAndExpire();
	}

	/**
	 * Decode a LTPA v1 or v2 token
	 * 
	 * @param tokenLTPA
	 * @param version
	 * @return
	 * @throws Exception
	 */
	public UserMetadata decodeLTPATokenUser(String tokenLTPA, LTPA_VERSION version) throws Exception {
		// lets get the shared key
		byte[] sKey = getSecretKey(this.sharedKey, this.keyPassword);
		// and decode from base64 to bytes the given token
		byte[] encryptedBytes = base64.decode(tokenLTPA);
		// to get the plain decrypted token after applying the decrypting
		// algorithm
		String plainToken = new String(crypt(encryptedBytes, sKey,
				version.equals(LTPA_VERSION.LTPA2) ? CRIPTING_ALGORITHM.AES_DECRIPTING_ALGORITHM
						: CRIPTING_ALGORITHM.DES_DECRIPTING_ALGORITHM,
				Cipher.DECRYPT_MODE), StandardCharsets.UTF_8);
		// finally, lets parse the decrypted token into the user metadata
		return new UserMetadata(plainToken, version);
	}

}
