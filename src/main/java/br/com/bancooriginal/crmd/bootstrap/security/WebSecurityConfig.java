package br.com.bancooriginal.crmd.bootstrap.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		// Permite apenas requisição POST no login e para qualquer outra requisição, exige Autenticação
//		httpSecurity.csrf().disable().authorizeRequests().antMatchers(HttpMethod.POST, "/login").permitAll()
//				.anyRequest().authenticated().and()
//
//				// filtra requisições de login
//				.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
//						UsernamePasswordAuthenticationFilter.class)
//
//				// filtra outras requisições para verificar a presença do JWT no header
//				.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		httpSecurity.csrf().disable().authorizeRequests().anyRequest().permitAll();
		super.configure(httpSecurity);

	}

}
